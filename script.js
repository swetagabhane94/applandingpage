document.addEventListener("DOMContentLoaded", function() {
    const menuToggle = document.querySelector(".menu-toggle");
    const closeBtn = document.querySelector(".close-btn");
    const headerLink = document.querySelector(".header-links");

    menuToggle.addEventListener("click", () => {
        headerLink.classList.add("open");
    });

    closeBtn.addEventListener("click", () => {
        headerLink.classList.remove("open");
    });
});

document.addEventListener("DOMContentLoaded", () => {
    const switchModeButton = document.querySelector(".switch-mode");
    const body = document.body;
    const icon = switchModeButton.querySelector("i");
    const logoImg = document.querySelector(".logo-header img");
    const footerImg = document.querySelector(".contact-left-footer img");


    switchModeButton.addEventListener("click", () => {
        body.classList.toggle("dark-mode");
        
        const allTextElements = document.querySelectorAll("body *");
        allTextElements.forEach(element => {
            element.classList.toggle("dark-text");
        });
        
        if (body.classList.contains("dark-mode")) {
            logoImg.src = "./images/dark-logo.png";
        } else {
            logoImg.src = "./images/logo.png";
        }

        if (body.classList.contains("dark-mode")) {
            footerImg.src = "./images/dark-logo.png";
        } else {
            footerImg.src = "./images/logo.png";
        }

        icon.classList.toggle("fa-moon");
        icon.classList.toggle("fa-sun");
    });
});
